/* 
How Much is True?
Create a function which returns the number of true values there are in an array.
*/

function countTrue(arr){
	return arr.filter(value => value == true).length;
}

/* 
Converting Objects to Arrays
Write a function that converts an object into an array, where each element represents a key-value pair in the form of an array.
*/

function toArray(obj) {
    return Object.entries(obj);
}
/* 
Find the nth Tetrahedral Number
A tetrahedron is a pyramid with a triangular base and three sides. A tetrahedral number is a number of items within a tetrahedron.
Create a function that takes an integer n and returns the nth tetrahedral number.
*/

function tetra(n){
    return (n * (n + 1) * (n + 2)) / 6;
}


/*
Burglary Series (04): Add its Name
Given three arguments ⁠— an object obj of the stolen items, the pet's name and a value ⁠— return an object with that name and value in it (as key-value pairs).
*/

function addName(obj, name, value) {
	obj[name] = value;
	return obj;
}

/*
Learn Lodash (2): Compact
According to the lodash documentation, _.compact creates an array with all falsey values removed. The values false, null, 0, "", undefined, and NaN are falsey.
Your task is to build this helper function without using lodash. You will write a function that receives an array and removes all falsey values.
*/
function compact(arr) {
    //const toDelete = [false,null,0,"",undefined,NaN];
    let filteredArr = arr.filter(Boolean);
    return filteredArr;
}


/*
Which Generation Are You ?

Try finding your ancestors and offspring with code.
Create a function that takes a number x and a character y ("m" for male, "f" for female), and returns the name of an ancestor (m/f) or descendant (m/f).
If the number is negative, return the related ancestor.
If positive, return the related descendant.
You are generation 0. In the case of 0 (male or female), return "me!".
*/

function generation(x, y) {

    const gen = {

        1: {
            m:"son",
            f:"daughter" 
        },
        2: {
            m:"grandson",
            f:"granddaughter" 
        },
        3: {
            m:"great grandson",
            f:"great granddaughter" 
        },
        "-1": {
            m:"father",
            f:"mother" 
        },
        "-2": {
            m:"grandfather",
            f:"grandmother" 
        },
        "-3": {
            m:"great grandfather",
            f:"great grandmother" 
        }
    }
    

	if(x == 0){
        return "me!";
    }else{
        return gen[x][y];
    }
    
}

/*
Seven Boom!
Create a function that takes an array of numbers and return "Boom!" if the digit 7 appears in the array. Otherwise, return "there is no 7 in the array".
*/
function sevenBoom(arr) {
    const regex = new RegExp('7');

    if(regex.test(arr)){
        return "Boom!";
    } else {
        return "there is no 7 in the array";
    }
}

/*
Calculate the Total Price of Groceries
Create a function that takes an array of objects (groceries) which calculates the total price and returns it as a number. A grocery object has a product, a quantity and a price, for example:
{
    "product": "Milk",
    "quantity": 1,
    "price": 1.50
}
*/

function getTotalPrice(groceries) {
    total = 0;
    let product_total = 0;
    groceries.forEach(element => {
        let quantity = element.quantity;
        let price = element.price;
        product_total = quantity * price;
        total += product_total;
    });
    
   return total = Math.floor(total * 100) / 100;
}


/*
Right Shift by Division
The right shift operation is similar to floor division by powers of two.

Sample calculation using the right shift operator ( >> ):

80 >> 3 = floor(80/2^3) = floor(80/8) = 10
-24 >> 2 = floor(-24/2^2) = floor(-24/4) = -6
-5 >> 1 = floor(-5/2^1) = floor(-5/2) = -3
Write a function that mimics (without the use of >>) the right shift operator and returns the result from the two given integers.
*/

function shiftToRight(x, y) {
	return Math.floor(x/Math.pow(2,y));
}


/*
Find Number of Digits in Number
Create a function that will return an integer number corresponding to the amount of digits in the given integer num.
*/

function num_of_digits(num) {
    if(Math.sign(num) == -1){
        return num.toString().length - 1;
    }else{
        return num.toString().length;
    }
}

/*
Length of a Nested Array
The .length property on an array will return the number of elements in the array. For example, the array below contains 2 elements:

[1, [2, 3]]
// 2 elements, number 1 and array [2, 3]
Suppose we instead wanted to know the total number of non-nested items in the nested array. In the above case, [1, [2, 3]] contains 3 non-nested items, 1, 2 and 3.

Write a function that returns the total number of non-nested items in a nested array.

Examples
getLength([1, [2, 3]]) ➞ 3

*/

function getLength(arr) {
	return arr.flat(Infinity).length;
}


/*
Concatenate Variable Number of Input Arrays
Create a function that concatenates n input arrays, where n is variable.
*/

function concat(...args) {
	return args = args.flat(Infinity);
}

/*
Burglary Series (12): Get Vodka Bottle
The insurance guy laughs, he's just kidding. He just needs an updated list. You just need one of those Rammstein Vodka bottles.
Given an object with alcoholic drinks and a number, return a string with the name of the Rammstein bottle that matches the given number.
*/

function getVodkaBottle(obj, num) {
	for(const [key,value] of Object.entries(obj)){
        if(key.startsWith("Rammstein") && value == num){
            return key;
        }
    }

    return null;
}


/*
Basic Arithmetic Operations on a String Number
Create a function to perform basic arithmetic operations that includes addition, subtraction, multiplication and division on a string number (e.g. "12 + 24" or "23 - 21" or "12 / 12" or "12 * 21").

Here, we have 1 followed by a space, operator followed by another space and 2. For the challenge, we are going to have only two numbers between 1 valid operator. The return value should be a number.

eval() is not allowed. In case of division, whenever the second number equals "0" return -1.

For example:

"15 / 0"  ➞ -1
*/


function arithmeticOperation(n) {
    let calcul = n.split(" ");
    let operator = 0;
    console.log(calcul);

    calcul.forEach(element => {

        if(Number(element) == NaN){
            operator = element;
        }
        
    });

    console.log(operator);
}

console.log(arithmeticOperation("12 + 12"));